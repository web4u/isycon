$(function () {

    $(window).scroll(function(e){

        var scrollTop = window.pageYOffset ? window.pageYOffset : (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);

        var sH=$(window).height();
    });

    $(".changeLang span").click(function () {
        $(".lang").slideToggle();
    });

    $(".lang li").click(function () {
        $(".changeLang span").html($(this).html());
        $(".lang").slideToggle();
    });

    $(".top-box").slick();

    $(".rulles .right").click(function (e) {
        e.preventDefault();
        $('.top-box').slick('slickNext');
    });

    $(".rulles .left").click(function (e) {
        e.preventDefault();
        $('.top-box').slick('slickPrev');
    });

    $('.top-box').on('afterChange', function(event, slick, currentSlide, nextSlide){
        $('.rulles ul li:eq('+currentSlide+')').addClass('active').siblings().removeClass('active');
    });

    $(".mobile-menu").click(function () {
        $("header ul").toggleClass('open');
        $(this).toggleClass('fa-bars fa-times active')
    });

    $(".text-slider").slick({
        prevArrow: '<div class="slider-left"><i class="fa fa-chevron-left"></i></div>',
        nextArrow: '<div class="slider-right"><i class="fa fa-chevron-right"></i></div>'
    });
});